        
            function onScrollAnimation() {
            
                var onScroll = document.getElementsByClassName("animate");
                
                for (var i = 0; i < onScroll.length; i++) {
            
                    if (onScroll[i].getBoundingClientRect().top < window.innerHeight - 150) {
                        onScroll[i].classList.add("active");
                    } else {
                        onScroll[i].classList.remove("active");
                    }
                }
            }
            
            window.addEventListener("scroll", onScrollAnimation);
        